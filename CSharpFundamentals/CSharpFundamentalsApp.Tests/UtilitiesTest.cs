using Xunit;

namespace CSharpFundamentalsApp.Tests
{
    public class UtilitiesTest
    {
        [Fact]
        public void RemoveVowels_Test()
        {
            // arrange
            string name = "Rajib Sarker";
            // act
            var newString = Utilities.RemoveVowel(name);
            // assert
            Assert.Equal("Rjb Srkr", newString);
        }
    }
}