﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentalsApp
{
    internal struct WorkingTask
    {
        public string description;
        public string hours;

        public void PerformWorkingTask()
        {
            Console.WriteLine($"Task {description} with {hours} hour's has been completed.");
        }
    }
}
