﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentalsApp
{
    internal class Employee
    {
        public string firstName;
        public string middleName;
        public string lastName;

        public string address;
        public string contactNo;

        public Employee(string firstName, string lastName, string address, string contactNo)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.contactNo = contactNo;
        }

        public Employee(string firstName, string middleName, string lastName, string address, string contactNo):this(firstName, lastName, address, contactNo)
        {
            this.middleName = middleName;
        }

        public string GetFullName()=> firstName + " " + middleName + " "+ lastName;

        public void EmployeeDetails()
        {
            Console.WriteLine($"Employee----> Name: {GetFullName()},\t Address: {address}, \tContact No: {contactNo}");
        }
    }
}
