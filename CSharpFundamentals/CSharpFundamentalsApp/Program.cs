﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");


//int max = int.MaxValue;
//int min = int.MinValue;

//char charVal = 'a';
//char upperVal = char.ToUpper(charVal);
//char lowerVal = char.ToLower(charVal);

//bool isDigit = char.IsDigit(upperVal);
//bool isLetter = char.IsLetter(lowerVal);

//// date time
//DateTime now = DateTime.Now;
//DateTime start = DateTime.Now;
//TimeSpan workingHour = new TimeSpan(8,30,0);
//DateTime end = start.Add(workingHour);

using CSharpFundamentalsApp;
using System.Text;

//Utilities.CalculateArea(5, 3);
//var data = Utilities.RemoveVowel("Rajib Sarker");
//Console.WriteLine(data);

//Console.WriteLine(Utilities.HideVowels("Rajib Sarker"));

//Employee emp1 = new Employee("Rajib", "Sarker", "Dhaka", "019933");
//Employee emp2 = new Employee("Rajib", "Sarker", "Raju", "Chandpur", "933333");

//..................................................String Builder.......................//////////
//string index = string.Empty;
//for(int i = 0; i<2500; i++)
//{
//    index+=i.ToString();
//}
//Console.WriteLine(index);

//StringBuilder st = new StringBuilder();
//st.Append("FirstName: ");
//st.AppendLine();
//st.AppendLine("Rajib Sarker");
//st.AppendLine("LastName: ");
//st.Append("Sarker");

//string t = st.ToString();
//Console.WriteLine("Usign string builder: "+t);


//emp1.EmployeeDetails();
//emp2.EmployeeDetails();

WorkingTask wt;
wt.description = "My Task";
wt.hours = "10";
wt.PerformWorkingTask();

//WorkingTask wt = new WorkingTask();
//wt.description = "New Task";
//wt.hours = "10";
//wt.PerformWorkingTask();


Console.ReadLine();