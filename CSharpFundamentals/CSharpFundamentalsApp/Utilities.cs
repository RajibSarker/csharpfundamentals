﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentalsApp
{
    public class Utilities
    {
        static string vowels = "AEIOUaeiou";
        public static void CalculateArea(double length, double width) => Console.WriteLine($"The Area Is: {length * width}");

        public static string RemoveVowel(string text)
        {
            string newString = "";
            foreach (char c in text)
            {
                if (!vowels.Contains(c))
                {
                    newString+= c;
                }
            }

            return newString;
        }

        public static string HideVowels(string text)
        {
            string newString = "";
            foreach (char c in text)
            {
                char st = c;
                if (vowels.Contains(c))
                {
                    st = '*';
                }
                newString += st;
            }
            return newString;
        }
    }
}
